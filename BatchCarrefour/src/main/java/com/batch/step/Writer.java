package com.batch.step;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
 
public class Writer<T> implements ItemWriter<T> {
 
  Logger logger = LoggerFactory.getLogger(this.getClass());
  
  @Override
  public void write(List<? extends T> items) throws Exception {
      for (T item : items) {
          System.out.println(item);
      }
  }
  
}