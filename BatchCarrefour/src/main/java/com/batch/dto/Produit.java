package com.batch.dto;

public class Produit implements Serializable{

	private String produit;
	private int prix;
	
	public Produit() {
	}

	public String getProduit() {
		return produit;
	}

	public void setProduit(String produit) {
		this.produit = produit;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "Produits [produit=" + produit + ", prix=" + prix "]";
	}
}
