package com.batch.step;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.batch.item.ItemProcessor;

import com.batch.dto.Transactions;

public class Processor implements ItemProcessor<String, String> {

	@Override
	public String process(String content) throws Exception {
		return content.toUpperCase();
	}

	private static List<Entry<String, Integer>> sortAsMap(Transactions t) {
	  
		final Map<String, Integer> map = new HashMap<String, Integer>();
		
		map.put(t.getProduit(), t.getQte());
		
		final List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(map.entrySet());
		 Collections.sort(entries, new Comparator<Entry<String, Integer>>() {
	    public int compare(final Entry<String, Integer> e1, final Entry<String, Integer> e2) {
	      return e1.getValue().compareTo(e2.getValue());
	    }
	  });
		
		return (entries);
	}
		
  

 

}