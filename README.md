|-- src     
|-- .gitignore
|   |-- batch.config
|       |-- batch.config
|       |-- BatchConfig
|   |-- batch.controller
|       |-- JobLauncherController
|       |-- repositories
|   |-- batch.dto
|       |-- Produit
|       |-- Transactions
|   |-- batch.step
|       |-- Processor
|       |-- Reader
|       |-- Writer
|   |-- batch.top
|       |-- BatchCarrefourApplication



BatchConfig: 
contenant la configuration pour la lecture des fichiers "datasource Batch for Job Repository": chemin des fichiers, lecture des fichiers et insertion dans les objets.

ItemReader
est utilisé pour obtenir des données par la méthode read ()


ItemProcessor
est utilisé pour transformer des données, où nous trouvons la méthode sortAsMap
dans le cas de top_100_ventes_GLOBAL_YYYYYMMDD.data donc la création d'une chaîne de hachage contenant :String  le nom du produit , Integer contenant la quantité
et boucler sur l'objet
Si le produit n'existe pas alors ajouter dans la carte en Hash avec sa quantité
Si le produit est en ligne et que vous en avez déjà besoin
Afficher les 100 premiers sorties

JobLauncherController : qui aident à utiliser et à manipuler le Job via ihm