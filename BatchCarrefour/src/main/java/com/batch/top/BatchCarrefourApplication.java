package com.batch.top;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableBatchProcessing
public class BatchCarrefourApplication {
	
	@Autowired
    JobLauncher jobLauncher;
      
    @Autowired
    Job job;
//  to launch a job
	public static void main(String[] args) {
		SpringApplication.run(BatchCarrefourApplication.class, args);
	}
    // to Schedule a job
	@Scheduled(cron = "0 */1 * * * ?")
    public void perform() throws Exception    {
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job, params);
    }

}
