package com.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.batch.dto.Transactions;
import com.batch.step.Processor;
import com.batch.step.Reader;
import com.batch.step.Writer;
 

 
@Configuration
public class BatchConfig {
 
    @Autowired
    public JobBuilderFactory jobBuilderFactory;
 
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("data/reference*.data")
    private Resource[] inputReference;
    @Value("data/transactions*.data")
    private Resource[] inputTransactions;
    
  @Bean
    public Job readPlatFilejob() {
        return jobBuilderFactory.get("readPlatFilejob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }
 
  @Bean
  public Writer<Transactions> writer()
  {
      return new Writer<Transactions>();
  }
  
    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<String, String> chunk(1)
                .reader(new Reader())
                .processor(new Processor())
                .writer(new Writer())
                .build();
    }
    // set reader MultiResources
    @Bean
    public MultiResourceItemReader<Transactions> multiResourceItemReader()
    {
        MultiResourceItemReader<Transactions> resourceItemReader = new MultiResourceItemReader<Transactions>();
        resourceItemReader.setResources(inputTransactions);
        resourceItemReader.setDelegate(reader());
        return resourceItemReader;
    }
     
    @Bean
    public FlatFileItemReader<Transactions> reader()
    {
        //Create reader instance
        FlatFileItemReader<Transactions> reader = new FlatFileItemReader<Transactions>();
         
        //Set number of lines to skips. Use it if file has header rows.
        //reader.setLinesToSkip(1);  
         
        //Configure how each line will be parsed and mapped to different values
        reader.setLineMapper(new DefaultLineMapper() {
            {
                //3 columns in each row
                setLineTokenizer(new DelimitedLineTokenizer() {
                    {
                        setNames(new String[] { "txId", "datetime", "magasin","produit","qte"});
                    }
                });
                //Set values in class
                setFieldSetMapper(new BeanWrapperFieldSetMapper<Transactions>() {
                    {
                        setTargetType(Transactions.class);
                    }
                });
            }
        });
        return reader;
    }
    
    
    
    
    
}
