package com.batch.dto;

import java.io.Serializable;

public class Transactions implements Serializable {
	private String txId;
    private String datetime;
    private String magasin;
	private String produit;
    private int qte;

    
    
    
    
	public Transactions() {
	}





	public String getTxId() {
		return txId;
	}





	public void setTxId(String txId) {
		this.txId = txId;
	}





	public String getDatetime() {
		return datetime;
	}





	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}





	public String getMagasin() {
		return magasin;
	}





	public void setMagasin(String magasin) {
		this.magasin = magasin;
	}





	public String getProduit() {
		return produit;
	}





	public void setProduit(String produit) {
		this.produit = produit;
	}





	public int getQte() {
		return qte;
	}





	public void setQte(int qte) {
		this.qte = qte;
	}





	@Override
	public String toString() {
		return "Transactions [txId=" + txId + ", datetime=" + datetime + ", magasin=" + magasin + ", produit=" + produit
				+ ", qte=" + qte + "]";
	}
	
	
    
  
}
